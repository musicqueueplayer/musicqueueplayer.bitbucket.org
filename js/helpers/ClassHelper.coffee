define [
	'jquery',
		'underscore',
		'backbone'
], ($, _, Backbone) ->

	getMissingKeys = (properties, requiredKeys) =>
		result = []
		for key in requiredKeys
			segments = key.split '.'
			propertyChain = properties
			for segment in segments
				propertyChain = propertyChain[segment]
				if !propertyChain
					result.push key
					break
		result

	ClassHelper =
		getMissingKeys: getMissingKeys

	ClassHelper